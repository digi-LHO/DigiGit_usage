# Digi Git Usage

It's here that you will find everything you need to know/do to participate to the Digi R&D, along with some good practices and tools to do things.
First of all : you **will** use git extensively, and not only to save stuff, but we should be able to see things evolve over time and even exists in multiple states.

## **Tools to install**

The simplest is the best : if you are not already a git great master, you want to avoid any kind of black magic ! (so do I, I don't want to un-destroy black magic's mistakes) You want the simplest tool possible : only plain and simple command lines and tools.

For linux, all you need to get is "git" and "git-gui"
which correspond exactly to what you will find here : http://git-scm.com for all other platforms.

## **Status**

At any time, if you need to know what is the state of your local repository you can use :

```
git status
```

### The graphical way

but you can also use the graphical way !
using the command

```
git gui
```

you will get something like :

![](image.png)

Here is an example of **what has changed** from **the last revision** of your repo you pulled from gitlab.
You notice that there are different file icons depending on what happens with the file.

More on that later.

## **Commit**
In order to **stage** modifications (including file creation/deletation) to your commit you can either 
```
git add a_file_created a_file_modified
git rm a_file_deleted
```
then creating the commit and (assuming you already are on the branch you want to push on) push
```
git commit -m "an explicit message for my commit"
git push
```
If you are not on the branch you want to push your modifs on you need to specify the target of your push by doig :
```
git push origin my_other_branch
```
Of course, at any time during that process, you can git status to see where you are at.

### The graphical way

You can also use the graphical way with you git gui :
Instead of "git add" and "git remove" you can **click** on the **small icons** I showed you earlier.

This will **stage** the files to you commit

Then you will fill the commit message, click **Commit** and **push**

![](image-1.png)

## **Tricks and tips**

### Commit part of a file only

Instead of staging a full file, you can commit only a part of a file, that can prove to be useful in case of config file for instance.
I won't explain the command line, but I'll show you the graphical way to do so :

![](image-2.png)

Once clicked, the file will appear both in **Unstaged** and **Staged** part of the UI :

![](image-3.png)


Here you go for the basics !

More tricks will follow !

