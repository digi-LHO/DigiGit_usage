# Branching

## Step 1 : Creating a new branch

If you already know that you will do some changes OR you already have done changes that may disturb the main branch (like make it unstable for instance). You can create a new branch.
To do so :

```$ git branch my_new_branch```

You have now 2 branches that are exactly the same.

![image-1.png](./image-1.png)

Note : ***At this stage, your newly created branch exist only locally on you PC !***

## Step 2 : Move from a branch to another

Your branch is created but you are still on master, you now need to :

```$ git checkout my_new_branch```

to move from master to my_new_branch.

Note : If you have uncommited changes, they will stay as you go from branch to another, so let's say you were working on master, but you don't want to commit your changes on it. You should create a branch or swap to an exisiting one THEN you create your commit.

## Step 3 : Make a commit on a branch

Ok ! Now we want to commit something : 

```$ git add file_1 file_2```

```$ git commit -m "My unstable commit"```

```$ git push origin my_new_branch ``` (this can also be used for master : ```$ git push origin master```)

![image.png](./image.png)

Note that I specified on wich branch I want to push my commit on, it's not mandatory BUT it will help you control precisely what you are doing, preventing you from making a mistake.


## Step 4 : Merging a branch into another

Let's imagine that the repo evolved such as this drawing :

![image-2.png](./image-2.png)

Both master and my_new_branch have evolved in parallel. (Here we assume that all our branches are in sync with git server)

Now I have finished my work on my_new_branch, I want to put it in master.

Since we are still on my_new_branch, first I switch to master

```$ git checkout master```

We are now on destination branch where we want to put our changes from my_new_branch on.

```$ git merge my_new_branch```

And that's it ! Assuming you don't have any conflicting changes, you are done !

You can now 

```git push origin master```

To push you merge on the server.

Your git history now look like this :

![image-4.png](./image-4.png)

You are done with the merge !

## Step 5 : Delete the merged branch

First you will delete your branch locally :

```$ git branch -d my_new_branch```

Then you will delete it remotely on the server

```$ git push origin --delete my_new_branch```

You will notice that the command is quite wordy, it's to prevent you from doing something bad by mistake.

We are done for this quick git branching tutorial !

But don't worry, there are still a lot of stuff to come later ;)

